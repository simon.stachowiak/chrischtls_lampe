#include <FastLED.h>
#include <IRremote.h>

//LED stuff
#define NUM_LEDS 60
#define LED_PIN 10
#define RGB_ORDER GRB
#define MAX_BRIGHTNESS 105
#define THRESHOLD 100

//Controls
#define IR_PIN 9
#define INTERRUPT_PIN 2


//IR REMOTE KEYS
#define KEY_0 16750695
#define KEY_1 16753245
#define KEY_2 16736925
#define KEY_3 16769565
#define KEY_4 16720605
#define KEY_5 16712445
#define KEY_6 16761405
#define KEY_7 16769055
#define KEY_8 16754775
#define KEY_9 16748655
#define KEY_HASH 16756815
#define KEY_STAR 16738455
#define KEY_UP 16718055
#define KEY_DOWN 16730805
#define KEY_LEFT 16716015
#define KEY_RIGHT 16734885
#define KEY_OK 16726215

//PROGRAMS
#define PROGRAM_SPARKLE 0
#define PROGRAM_STATIC_WHITE 1
#define PROGRAM_STATIC_GREEN 2
#define PROGRAM_STATIC_ORANGE 3
#define PROGRAM_GREEN_PULSE 4
#define PROGRAM_STATIC_RED 5
#define PROGRAM_GREEN_FADE 6
#define PROGRAM_RAINBOW 7
#define PROGRAM_RAVEBOW 8
#define PROGRAM_RANDOM_COLOR 9

//COLORS
#define COLOR_GREEN 0x00DD00
#define COLOR_GREENEST 0x00FF00
#define COLOR_LIGHT_GREEN 0x75EE00
#define COLOR_DARK_GREEN 0x006F00
#define COLOR_WHITE 0xFFFFFF
#define COLOR_WARM_WHITE 0xFDF5E6
#define COLOR_ORANGE 0xFF8C00
#define COLOR_AQUA 0x66CDAA
#define COLOR_BLUE 0x00EEEE
#define COLOR_BLUEST 0x0000FF
#define COLOR_RED 0xCD0000
#define COLOR_REDDEST 0xFF0000
#define COLOR_GOLD 0xEEC900
#define COLOR_PURPLE 0x9F30FF



CRGB leds[NUM_LEDS];
long tick[NUM_LEDS];

//IR Thingis
IRrecv receiver(IR_PIN);
decode_results results;
unsigned long remote_key = 0;

//Control values
int currentProgram = PROGRAM_STATIC_WHITE;
int brightness = 255;
int rnd;
int rndtick;

bool buttonPressed = false;
bool lightsOn = true;
bool sparklesOn = false;

void handleInterrupt()
{
  buttonPressed = true;
}

void setup() 
{
  Serial.begin(9600);
  //Setup LEDs
  FastLED.addLeds<WS2812B, LED_PIN, RGB_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(brightness);
  
  //Setup Infrared Receiver
  receiver.enableIRIn();

  //attatch Interrupt
  pinMode(INTERRUPT_PIN,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN),handleInterrupt,FALLING);

  updateProgram(currentProgram); 
}

void loop() {
 //Read IR Remote
  if(receiver.decode(&results))
  {
      remote_key = results.value;
      Serial.println(remote_key);
      receiver.resume();
      Serial.println(remote_key);

      switch(remote_key)
      {
          case KEY_OK:
            lightsOn = !lightsOn;
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
            sparklesOn = false;
            break;
          case KEY_0:
            currentProgram = PROGRAM_SPARKLE;
            sparklesOn = true;
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
=======
            break;
          case KEY_0:
            currentProgram = PROGRAM_SPARKLE;
>>>>>>> 78ec7c422ebd385c289aeafec4b18a78ad6614b6:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
            Serial.println("Program: Golden Sparkles");
            break;
          case KEY_1:
            currentProgram = PROGRAM_STATIC_WHITE;
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
            sparklesOn = false;
=======
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
            sparklesOn = false;
=======
>>>>>>> 78ec7c422ebd385c289aeafec4b18a78ad6614b6:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
            Serial.println("Program: Static White");
            break;
          case KEY_2:
            currentProgram = PROGRAM_STATIC_GREEN;
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
            sparklesOn = false;
=======
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
            sparklesOn = false;
=======
>>>>>>> 78ec7c422ebd385c289aeafec4b18a78ad6614b6:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
            Serial.println("Program: Static Green");
            break;
          case KEY_3:
            currentProgram = PROGRAM_STATIC_ORANGE;
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
            sparklesOn = false;
            break;
          case KEY_4:
            currentProgram = PROGRAM_GREEN_PULSE;
            sparklesOn = false;
            break;
          case KEY_5:
            currentProgram = PROGRAM_STATIC_RED;
            sparklesOn = false;
            break;
          case KEY_6:
            currentProgram = PROGRAM_GREEN_FADE;
            sparklesOn = false;
            break;
          case KEY_7:
            currentProgram = PROGRAM_RAINBOW;
            sparklesOn = false;
            break;
          case KEY_8:
            currentProgram = PROGRAM_RAVEBOW;
            sparklesOn = false;
            break;
          case KEY_9:
            currentProgram = PROGRAM_RANDOM_COLOR;
            sparklesOn = false;
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
=======
            break;
          case KEY_4:
            currentProgram = PROGRAM_GREEN_PULSE;
            break;
          case KEY_5:
            currentProgram = PROGRAM_STATIC_RED;
            break;
          case KEY_6:
            currentProgram = PROGRAM_GREEN_FADE;
            break;
          case KEY_7:
            currentProgram = PROGRAM_RAINBOW;
            break;
          case KEY_8:
            currentProgram = PROGRAM_RAVEBOW;
            break;
          case KEY_9:
            currentProgram = PROGRAM_RANDOM_COLOR;
>>>>>>> 78ec7c422ebd385c289aeafec4b18a78ad6614b6:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
            break;
              
            //Chooses a random programm above or below current program (hash is above, star is below)
          case KEY_HASH:
            rnd = random8(currentProgram,9);
            currentProgram = rnd;
            break;
          case KEY_STAR:
            rnd = random8(0, currentProgram);
            currentProgram = rnd;
            break;
              
            //BASIC CONTROL!!! 
          case KEY_RIGHT:
            if(currentProgram < 9)
            {
              currentProgram++;
            }
            else
            {
              currentProgram = 0;
            }
            break;
          case KEY_UP:
            if(brightness < MAX_BRIGHTNESS)
            {
              brightness += MAX_BRIGHTNESS/5;
            }
            break;
          case KEY_DOWN:
            if(brightness > 0)
            {
              brightness -= MAX_BRIGHTNESS/5;
            }
            break;
          case KEY_LEFT:
            if(currentProgram > 0)
              {
                currentProgram--;
              }
              else
              {
                currentProgram = 9;
              }
            break;
          default:
            Serial.println("Invalid Key");
            return;
        }
        if(lightsOn)
        {
          FastLED.setBrightness(brightness); 
        }
        else
        {
          FastLED.setBrightness(0);
        }
        updateProgram(currentProgram);
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
        if(sparklesOn)
        {
          program_sparkle();
        }
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
=======
>>>>>>> 78ec7c422ebd385c289aeafec4b18a78ad6614b6:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
  }

 //check Interrupt
 if(buttonPressed)
 {
    Serial.println("Button Pressed!");
    buttonPressed = false;
    //Do Interrupt stuff
    if(currentProgram < 9)
    {
      currentProgram++;
      updateProgram(currentProgram);
    }
    else if(currentProgram == 9 && lightsOn)
    {
      lightsOn = false;
    }
    else if(currentProgram == 9 && !lightsOn)
    {
      lightsOn = true;
      currentProgram = 0;
      updateProgram(currentProgram);
    }
 }  
}

//Programs are made here:

void updateProgram(int program)
{
  Serial.print("Program: ");
  Serial.println(program);
  switch(program)
  {
    case PROGRAM_STATIC_WHITE :
      Serial.println("Color: Warm White");
      program_white();
      break;
    case PROGRAM_STATIC_GREEN :
      Serial.println("Color: Green");
      program_green();
      break;
    case PROGRAM_STATIC_ORANGE :
      Serial.println("Color: ORANGE");
      program_orange();
      break;
    case PROGRAM_STATIC_RED :
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
      Serial.println("Color: Red");
=======
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
      Serial.println("Color: Red");
=======
    Serial.println("Color: Red");
>>>>>>> 78ec7c422ebd385c289aeafec4b18a78ad6614b6:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
      program_red();
      break;
    case PROGRAM_RAVEBOW :
      Serial.println("Color: Ravebow");
      break;
    case PROGRAM_RAINBOW :
      Serial.println("Color: Rainbow");
      break;
    case PROGRAM_SPARKLE :
      Serial.println("Color: Gold Sparkle");
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
      program_sparkle();
>>>>>>> 78ec7c422ebd385c289aeafec4b18a78ad6614b6:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
      break;
    case PROGRAM_GREEN_FADE :
      Serial.println("Color: 50 shades of Green");
      break;
    case PROGRAM_GREEN_PULSE :
      Serial.println("Color: Moar Green damnit");
      break;
    case PROGRAM_RANDOM_COLOR :
      Serial.println("Color: Who knows");
      break;
    default: return;
  }
}

void program_white()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_WARM_WHITE;
    FastLED.show();
  }
}

void program_green()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_GREENEST;
    FastLED.show();
  }
}

void program_orange()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_ORANGE;
    FastLED.show();
  }
}

void program_red()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_REDDEST;
    FastLED.show();
  }
}

void program_sparkle()
{
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
<<<<<<< HEAD:sketch_aug15a/sketch_aug15a.ino
=======
  sparklesOn = true;
>>>>>>> 78ec7c422ebd385c289aeafec4b18a78ad6614b6:sketch_aug15a/sketch_aug15a.ino
>>>>>>> e3cf58913b7d267d5cd7d14e580e9e6e287d5e5c:sketch_aug15a/sketch_aug15a.ino
  if(sparklesOn){
    for(int i = 0; i < NUM_LEDS; i++)
    {
      rndtick = random8(1, 11);
      tick[i] += rndtick;
      Serial.println(leds[i]);
      Serial.println(" hat einen tick von ");
      Serial.println(tick[i]); 
      if(tick[i] >= THRESHOLD)
      {
        leds[i] = COLOR_GOLD;
        tick[i] = 0;
        FastLED.show();
        Serial.println(leds[i]);
        Serial.println(" hat den threshold ueberschritten und leuchtet.");
      } else 
      {
        leds[i] = 0x000000;
      }
    }
  } 
}
